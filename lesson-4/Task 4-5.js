/**
 * Задача 5.
 *
 * Напишите функцию `createArray`, которая будет создавать массив с заданными значениями.
 * Первым параметром функция принимает значение, которым заполнять массив.
 * А вторым — количество элементов, которое должно быть в массиве.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента были переданы не число, не строка, не объект и не массив;
 * - В качестве второго аргумента был передан не число.
 */

// Решение

function createArray(value, length) {
    if (typeof value !== 'number' && typeof value !== 'string' && typeof value !== 'object' && !Array.isArray(value)) {
        throw new Error('В качестве первого элементо нужно передавать либо число, либо строку, либо объект, либо массив')
    }
    if (typeof length !== 'number') {
        throw new Error('В качестве второго аргумента нужно передавать число')
    }

    let arr = [];
    for (let i = 0; i < length; i++) {
        arr.push(value);
    }
    return arr;
}


const result = createArray('x', 5);

console.log(result); // [ x, x, x, x, x ]

// exports.createArray = createArray;