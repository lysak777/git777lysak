/**
 * Задача 1.
 *
 * Вручную создать имплементацию функции `filter`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенный метод Array.prototype.filter использовать запрещено.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция.
 *
 * Заметки:
 * - Второй аргумент встроенного метода filter (thisArg) имплементировать не нужно.
 */

const array = ["Доброе утро!", "Добрый вечер!", 3, 512, "#", "До свидания!"];

function filter(array, cb) {
    if (!Array.isArray) {
        throw new Error('Первый элемент должен быть массив')
    }

    if (typeof cb !== 'function') {
        throw new Error('Второй элемент должен быть функцией')
    }
    const filteredArray = [];

    for (let i = 0; i < array.length; i++) {
        const element = array[i];
        const isTrue = cb(element, i, array);
        if (isTrue) {
            filteredArray.push(element);
        }
    }
    return filteredArray;
}

const filteredArray = filter(array, (element, index, arrayRef) => {
//   console.log(`${index}:`, element, arrayRef);
    return element === "Добрый вечер!";
});

console.log(filteredArray); 