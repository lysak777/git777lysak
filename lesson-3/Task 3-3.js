// Задание 3

function truncate(string, maxLength) {
    return (string.length > maxLength) ?
        string.slice(0, maxLength - 3) + '…' : string;
}

console.log(truncate('Вот, что мне хотелось бы сказать на эту тему', 21))
// 'Вот что мне хотел...'