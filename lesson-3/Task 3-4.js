// Задача 4

const PRICE = "$120";

function extractCurrencyValue(source) {
    if (typeof source === 'string') {
        return (+source.slice(1));
    } else {
        return null;
    }
}


console.log(extractCurrencyValue(PRICE)); // 120
console.log(typeof extractCurrencyValue(PRICE)); //number
console.log(extractCurrencyValue({})); // null