// Задача 2

function checkSpam(source, spam) {
    if (typeof source !== 'string' || typeof spam !== 'string') {
        throw new Error('source и spam должны быть строками');
    }
    const result = source.toLowerCase().includes(spam.toLowerCase());
    return result;
}
console.log(checkSpam('pitterXXX@gmail.com', 'xxx')) // true
console.log(checkSpam('pitterxxx@gmail.com', 'XXX')) // true
console.log(checkSpam('pitterxxx@gmail.com', 'sss')) // false