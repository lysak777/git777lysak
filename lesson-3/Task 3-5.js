// Задача 5

const person = {};
Object.writable === true
Object.configurable === false

Object.defineProperty(person, "rate", {
    value: 25,
});

Object.defineProperty(person, "hours", {
    value: 4,
});

Object.defineProperty(person, "salary", {
    get: function () {
        return "Зарпалата за проект составляет " + this.rate * this.hours + "$";
    },
});

console.log(person.salary)


console.log(person.salary)