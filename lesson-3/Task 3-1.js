// Задача 1

function upperCaseFirst(str) {
    const result = str;
    if (typeof result !== 'string') {
        return result;
    }
    if (!result) {
        return result;
    }
    return str[0].toUpperCase() + str.slice(1);
}

console.log(upperCaseFirst('pitter'));
console.log(upperCaseFirst(''));
console.log(upperCaseFirst(1));